﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;


namespace ProyectoNetCore.Models
{
    public class Categorias
    {
        [Key]
        public int CategoriaID { get; set; }
        [Required(ErrorMessage ="Parametro Obligatorio")]
        [StringLength(50, MinimumLength =3, ErrorMessage ="El nombre debe poseer de 3 a 50 Caracteres")]
        public string Nombre { get; set; }
        [StringLength(256,ErrorMessage ="La descripcion  no debe exceder de 256 caracteres")]
        [Display(Name ="Descripcion")]
        public string descripcion { get; set; }
        public bool Estado { get; set; }


    }
}
