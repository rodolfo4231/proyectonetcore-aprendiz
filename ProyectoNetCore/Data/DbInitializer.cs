﻿using ProyectoNetCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoNetCore.Data
{
    public class DbInitializer
    {
        public static void Initialize(ProyectoNetCoreContext context)
        {
            context.Database.EnsureCreated();

            // validar la existencia de registros en la tabla categoria //

            if (context.Categoria.Any())
            {
                return;
            }

            var categorias = new Categorias[]
            {
                new Categorias{Nombre="Programacion",descripcion="Cursos de Programacion",Estado=true},
                new Categorias{Nombre="Diseño Grafico",descripcion="Cursos de Diseño Grafico",Estado=true}

            };

            foreach (Categorias c in categorias)
            {
                context.Categoria.Add(c);
            }
            context.SaveChanges();
        }
             
    }

 }

    

