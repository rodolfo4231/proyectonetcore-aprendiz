﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ProyectoNetCore.Models
{
    public class ProyectoNetCoreContext : DbContext
    {
        public ProyectoNetCoreContext (DbContextOptions<ProyectoNetCoreContext> options)
            : base(options)
        {
        }

        public DbSet<ProyectoNetCore.Models.Categorias> Categoria { get; set; }
    }
}
